// import thư viện mongoose
const mongoose = require("mongoose");
//class schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class schema
const courseSchema = new Schema ({
    courseCode: {
        type: String,
        unique: true,
        required: true
    },
    courseName: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    discountPrice: {
        type: Number,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    level: {
        type: String,
        required: true
    },
    coverImage: {
        type: String,
        required: true
    },
    teacherName: {
        type: String,
        required: true
    },
    teacherPhoto: {
        type: String,
        required: true
    },
    isPopular: {
        type: Boolean,
        default: 1
    },
    isTrending: {
        type: Boolean,
        default: 0
    }  
});
// Biên dịch course Model từ courseSchema
module.exports = mongoose.model("Course", courseSchema);
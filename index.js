// Import thư viện express JS
const express = require("express");
//import thư viện path
const path = require ("path");

// Khai báo thư viện Mongoose
const mongoose = require("mongoose");


//tạo app 
const app = express();

//khai báo cổng chạy app
const port = 8000;

// Cấu hình request đọc được body json
app.use(express.json());
// Khai báo router app
const courseRouter = require("./app/routes/courseRouter");

//call api chạy project course 365
app.get("/",(request, response) =>{
    response.sendFile(path.join(__dirname + "/views/index.html"))
})
//hiển thị hình ảnh cần thêm  middleware static vào express
app.use(express.static(__dirname +"/views"));

//kết nối với mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course365", (error) => {
    if(error) throw error;
    console.log("Connect course 365 to MongoDB successfully!");
})

//app sử dụng router
app.use("/api", courseRouter);

//chạy app trên port
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})